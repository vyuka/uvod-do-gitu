Diktáty z https://www.umimecesky.cz/diktaty

| Kód studenta | Vypracovaná úloha    | Code review |
| ------------ | -------------------- | ----------- |
| st86158      | 03_vyjmenovana_po_m.txt| st86162   |
| st91938      | 13_psani_me_mne.txt  | J. Matoušek (https://gitlab.com/vyuka/uvod-do-gitu/-/merge_requests/86) |
| st88753      | 05_vyjmenovana_po_s.txt | st91910     |
| st86162      | 01_vyjmenovana_po_b.txt| st86158   |
| st91910      | 10_koncovky_podstatnych_jmen.txt   | st88753     |
| st91940      | 04_vyjmenovana_po_p.txt  | nikdo - viz mail |
| st92604      | 18_velka_pismena_1.txt  | st91924     |
| st91900      | 07_koncovky_ovi_ovy.txt| st92598    |
| st92605      | 06_vyjmenovana_po_z.txt | st91916  |
| st92598      | 08_shoda_podmetu_s_prisudkem.txt   | st91900     |
| st91916      | vyplnění 06_vyjmenovana_po_v| st92605 |
| st91924      | 19_velka_pismena_2.txt | st92604     |
| st91909      | 02_vyjmenovana_po_l  | st91940     |
| st91928      | 09_koncovky_pridavnych_jmen.txt | -     |
| st91920      | 14 psani_n_nn.txt    | st91909     |

### Požadavky k získání zápočtu

1. Vytvořit issue, pomocí kterého si zarezervujete konkrétní zadání
2. Vytvořit fork projektu
3. Fork lokálně naklonovat
4. Lokáně vytvořit větev dle čísla a názvu přiřazeného issue
5. Zapracovat změny (s tím, že úmyslně vytvoříte 3-4 chyby)
6. Pushnut změny do svého forku
7. Vytvořit MR (merge request) ze své větvé do větve `master` v `upstream` repozitáři
8. Vyberete si jedno otevřené MR (svého kolegy) a uděláte code review
9. Počkáte, než vám někdo udělá CR a následně chyby z něj opravíte a pushnete (do svého forku)
